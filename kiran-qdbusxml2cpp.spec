Name:		kiran-qdbusxml2cpp
Summary:	Produces the C++ code to implement the dbus interfaces defined in the input file
Version:	2.2.0
Release:	5.kb4

License:    GPL-3.0-or-later
Source0:	%{name}-%{version}.tar.gz

Patch0001:      0000-docs-add-license-add-gpl3-except-license-d4b832e7.patch

BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel

Requires: qt5-qtbase

%description
%{summary}.

%prep
%autosetup -p 1

%build
%{__mkdir} -p %{buildroot}
%cmake
make %{?_smp_mflags}

%install
%make_install

%files
%doc
%{_bindir}/kiran-qdbusxml2cpp
%{_libdir}/cmake/KiranDBusGenerate

%changelog
* Tue Aug 09 2022 luoqing <luoqing@kylinsec.com.cn> - 2.2.0-5.kb4
- KYOS-F: Modify license.

* Sat Jan 22 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.0-4.kb4
- KYOS-B: Add BuildRequires: qt5-qtbase-private-devel

* Tue Jan 11 2022 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-4.kb3
- rebuild for KY3.4-5-KiranUI-2.2

* Tue Jan 11 2022 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-4.kb2
- rebuild for KY3.4-5-KiranUI-2.2

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 2.2.0-4.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 2.2.0-4
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb7
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb6
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb5
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb4
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb3
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-3.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Wed Oct 20 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-3.kb1
- KYBD: rebuild

* Wed Oct 20 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-2.kb1
- KYOS-B: add license

* Thu Aug 26 2021 liuxinhao <liuxinhao@kylinos.com.cn> - 2.2.0-1.kb1
- Initial build
